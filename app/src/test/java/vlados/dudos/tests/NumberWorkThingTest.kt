package vlados.dudos.tests

import org.junit.Test

internal class NumberWorkThingTest {

    private val numberWorkThing = NumberWorkThing(1536)

    @Test
    fun getLengthOfNumber() {
        assert(numberWorkThing.getLengthOfNumber() == 4)
    }

    @Test
    fun getNumeralByPosition() {
        assert(numberWorkThing.getNumeralByPosition(4) == 6)
    }

    @Test
    fun getNumberInStringFormat() {
        assert(numberWorkThing.getNumberInStringFormat() == "1536")
    }

    @Test
    fun getNumeralCharByPosition() {
        assert(numberWorkThing.getNumeralCharByPosition(3) == '3')
    }
}