package vlados.dudos.tests

class NumberWorkThing(private val number: Int) {

    fun getLengthOfNumber(): Int {
        return this.number.toString().length
    }

    fun getNumeralByPosition(position: Int): Int {
        return this.number.toString().split("")[position].toInt()
    }

    fun getNumberInStringFormat(): String {
        return this.number.toString()
    }

    fun getNumeralCharByPosition(position: Int): Char {
        return this.number.toString().split("")[position][0]
    }
}